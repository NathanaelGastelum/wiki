from flask import Flask, render_template, request, redirect
from bleach.sanitizer import Cleaner
from datetime import datetime
import os
import pathlib
import markdown
import subprocess
import csv

app = Flask(__name__)
page_dir = pathlib.Path(__file__).parent
allowed_tags = [
    "a",
    "abbr",
    "acronym",
    "address",
    "b",
    "br",
    "div",
    "dl",
    "dt",
    "em",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "hr",
    "i",
    "img",
    "li",
    "ol",
    "p",
    "pre",
    "q",
    "s",
    "small",
    "strike",
    "strong",
    "span",
    "sub",
    "sup",
    "table",
    "tbody",
    "td",
    "tfoot",
    "th",
    "thead",
    "tr",
    "tt",
    "u",
    "ul",
]

allowed_attrs = {
    "a": ["href", "target", "title"],
    "img": ["src", "alt", "width", "height"],
}


cleaner = Cleaner(tags=allowed_tags, attributes=allowed_attrs)


@app.route("/")
def main():
    page_list = get_page_list()
    return render_template(
        "main.html",
        page_name="wikiBREW",
        page_content="A hub of information for home coffee enthusiasts.",
        page_list=page_list,
    )


@app.route("/view/", defaults={"page": "Coffee"})
@app.route("/view/<page>")
def handle_request(page: str):
    page_cont = get_page_content(page)
    # convert markdown to html
    page_cont = convert_markdown_to_html(page_cont)
    # sanitize html
    page_cont = sanitize_html(page_cont)
    return render_template("page.html", page_name=page, page_content=page_cont)


def get_page_content(page):
    directory = f"{page_dir}/pages/{page}.md"
    with open(directory, "r") as f:
        content = f.read()
    f.close
    return content


def convert_markdown_to_html(markdown_txt):
    return markdown.markdown(markdown_txt)


def sanitize_html(html_txt):
    sanitized = cleaner.clean(html_txt)
    return sanitized


def get_page_list():
    page_list = []
    for path in os.listdir(rf"{page_dir}/pages"):
        temp = path.split("/")
        page_list.append(temp[-1].replace(".md", ""))
    return page_list


# Gets edit page content
@app.route("/edit/<pageName>", methods=["GET"])
def get_edit(pageName):
    with open(f"pages/{pageName}.md", "r") as file:
        content = file.read()
    file.close

    return render_template("edit.html", pre_populate_txt=content)


# Creates new page and log files if it doesn't exist
def add_page(pageName):
    with open(f"pages/{pageName}.md", "x") as file:
        file.write(f"{pageName}")
    open(f"history/{pageName}.log", "x")


# Post edit page form to page file
@app.route("/edit/<pageName>", methods=["POST"])
def post_edit(pageName):
    formContent = request.form["content"]
    formName = request.form["editor_name"]
    formEmail = request.form["editor_email"]
    formDesc = request.form["description"]

    update_page(pageName, formContent)

    subprocess.run(["git", "config", "user.email", f"{formEmail}"])
    subprocess.run(["git", "config", "user.name", f"{formName}"])
    subprocess.run(["git", "add", f"pages/{pageName}.md"])
    subprocess.run(["git", "commit", "--no-verify", "-m", f"{formDesc}"])

    logContent = subprocess.check_output(["git", "show", "-s", "--pretty=%H"])
    logContent = logContent.decode("utf-8").rstrip()

    with open(f"{page_dir}/history/{pageName}.log", "a") as file:
        file.write(
            f"'{formName} | {formEmail} | {formDesc} | {datetime.now()},{logContent}\n"
        )
    file.close

    return redirect(f"/view/{pageName}")


def update_page(pageName, newContent):
    with open(f"pages/{pageName}.md", "w") as file:
        file.write(newContent)


# Gets history and forms links to page file
# Checks to see if history file exists
@app.route("/history/<pageName>")
def get_history(pageName):
    if os.path.exists(f"history/{pageName}.log"):
        csv_file = open(f"history/{pageName}.log")
        csv_reader = csv.reader(csv_file, delimiter=",")

        links = []
        for line in csv_reader:
            links.append((line[0], f"/log/{line[1]}"))

        return render_template(
            "history.html", page_name=pageName, lines=links, linksArr=links
        )
    else:
        content = "No History Has Been Found For This Page!"
        return render_template("main.html", page_content=content)


# Gets commit logs according to commit hash SHA-1
# Writes commit log to temp.log file for proper format
@app.route("/log/<hash>")
def get_log(hash):
    with open(f"{page_dir}/history/Temp.log", "w") as file:
        logContent = subprocess.check_output(
            [
                "git",
                "show",
                "-s",
                "--patch-with-stat",
                "--pretty=Author: %aN %nEmail: %aE %nDate: %ad %n %n   %s ",
                "{0}".format(hash),
            ]
        )
        logContent = logContent.decode("utf-8").rstrip()
        file.write(logContent)
    file.close
    return render_template("log.html", history_log=logContent)


@app.route("/api/v1/pages/<page_name>/get")
def page_api_get(page_name):
    format = request.args.get("format", "all")
    if os.path.exists(page_dir / f"pages/{page_name}.md"):
        if format == "raw":
            json_response = {"success": True, "raw": get_page_content(page_name)}
            status_code = 200
        elif format == "html":
            markdown = get_page_content(page_name)
            html = convert_markdown_to_html(markdown)
            html = sanitize_html(html)
            json_response = {"success": True, "html": html}
            status_code = 200
        elif format == "all":
            markdown = get_page_content(page_name)
            html = convert_markdown_to_html(markdown)
            html = sanitize_html(html)
            json_response = {"success": True, "raw": markdown, "html": html}
            status_code = 200
        else:
            json_response = {"success": False, "reason": "Unsupported format"}
            status_code = 400
    else:
        json_response = {"success": False, "reason": "Page does not exist"}
        status_code = 404

    return json_response, status_code


@app.route("/add/", methods=["GET", "POST"])
def new_page():
    if request.method == "POST":
        page_title = request.form["page_title"]
        formContent = request.form["content"]
        formName = request.form["editor_name"]
        formEmail = request.form["editor_email"]
        formDesc = request.form["description"]

        update_page(page_title, formContent)

        subprocess.run(["git", "config", "user.email", f"{formEmail}"])
        subprocess.run(["git", "config", "user.name", f"{formName}"])
        subprocess.run(["git", "add", f"pages/{page_title}.md"])
        subprocess.run(["git", "commit", "--no-verify", "-m", f"{formDesc}"])

        logContent = subprocess.check_output(["git", "show", "-s", "--pretty=%H"])
        logContent = logContent.decode("utf-8").rstrip()

        with open(f"{page_dir}/history/{page_title}.log", "a") as file:
            file.write(
                f"'{formName} | {formEmail} | {formDesc} | {datetime.now()}'',{logContent}\n"
            )
        file.close
        return redirect(f"/view/{page_title}")
    else:
        return render_template("add.html")


if __name__ == "__main__":
    app.run()  # pragma: no cover
